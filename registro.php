<!DOCTYPE html>
<html>
<head>
<link href="css/datepicker.min.css" rel="stylesheet" type="text/css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="js/datepicker.min.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/i18n/datepicker.es.js"></script>
    <script src="js/comuna-region.js"></script>
    <script type="text/javascript" src="js/run2.js"></script>
    <script type="text/javascript" src="js/validacion.js"></script>
</head>
<body>
<?php include 'header.php' ?>

<div class="col-md-8 offset-md-2 card">
        
        <form action="" method="" id="formulario" name="" onsubmit="return validacion()">
        <div class="card-header"><h1 class="titulo">Ingrese sus Datos</h1></div>
        
        <div class="row">
            <div class="col-md-6">
            <label for="">Correo electrónico:</label>
            <input class="form-control" type="email" id="email" placeholder="Ingrese email" required><br/>
            </div>
            <div class="col-md-6">
            <label for="">Run:</label>
            <input class="form-control" type="text" id="txt_rut" placeholder="Ingrese su run" required><br/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
            <label for="">Nombres:</label>
            <input class="form-control" type="text" id="nombres" placeholder="Ingrese sus nombres" required><br/>
            </div>
            <div class="col-md-6">
            <label for="">Apellidos:</label>
            <input class="form-control" type="text" id="apellidos" placeholder="Ingrese sus apellidos" required><br/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
            <label for="">Fecha de nacimiento:</label>
            <input class="form-control" type="date" name="cumpleanios" step="1" max="2000-12-31" value="<?php echo date("2000-12-31");?>" required><br/>
            </div>
            <div class="col-md-6">
            <label for="">Teléfono de contacto:</label>
                <input class="form-control" type="text" id="contacto" maxlength="9" onkeypress="return valida(event)" placeholder="Ingrese teléfono celular" required><br/>
            </div>
        </div>    
            <script>
                function valida(e){
                    tecla = (document.all) ? e.keyCode : e.which;

                    //Tecla de retroceso para borrar, siempre la permite
                    if (tecla==8){
                        return true;
                    }
                        
                    // Patron de entrada, en este caso solo acepta numeros
                    patron =/[0-9]/;
                    tecla_final = String.fromCharCode(tecla);
                    return patron.test(tecla_final);
                }
            </script>
        
        <div class="row">
            <div class="col-md-6">
            <label for="">Región:</label><br/>
                <select class="form-control" id="regiones" required></select><br/>
            </div>
            <div class="col-md-6">
            <label for="">Ciudad:</label>
                <select class="form-control" id="comunas" required></select><br/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
            <label for="">Tipo de vivienda:</label>
                        <select class="form-control col-md-4" name="viviendaCb" required>
                            <option>Casa con patio grande</option> 
                            <option>Casa con patio pequeño</option> 
                            <option>Casa sin patio</option>
                            <option>Departamento</option>
            </div><br/>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <input type="submit" id="btnvalida" value="Enviar" class="btn btn-success"/><br/>
            </div>
        </div>
        </form>
  
  
    </div>
</body>
</html>