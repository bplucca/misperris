<!DOCTYPE html>
<html>
<body>
    <?php include 'header.php' ?>
    <div class="row">
        <div class="col-md-12 one-time banner">
            <div><img src="imagenes/adoptados/Apolo.jpg" alt="" class="img-fluid"></div>
            <div><img src="imagenes/adoptados/Duque.jpg" alt="" class="img-fluid"></div>
            <div><img src="imagenes/adoptados/Tom.jpg" alt="" class="img-fluid"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 banner2">
            <p>+56 9 98765431</p>
        </div>
        <div class="col-md-4 banner2">
            <p>Rescate y adopción de perros callejeros</p>
        </div>
        <div class="col-md-4 banner2">
            <p>botones</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 columnaI">
            <h1>RESCATE</h1>
            <H3>ETAPA UNO</H3>
            <HR>
            <P>Rescatamos perros en situación de peligro y/o abandono. Los rehabilitamos y los preparamos para buscarles un hogar.</P>
            <img src="imagenes/rescate.jpg" alt="">
        </div>
        <div class="col-md-6 columnaD">
            <img src="imagenes/crowfunding.jpg" alt="">
            <h1>CROWDFUNDING</h1>
            <H3>FINANCIAMIENTO</H3>
            <HR>
            <P>Sigue nuestras redes sociales para informarte acerca de las diversas campañas y actividades que realizamos para obtener financiamiento para seguir ayudando</P>
            <input type="submit" value="CAMPAÑAS" />
        </div>
    </div>

    <ul class="gallery_box">
        <li>
            <a href="#0"><img src="imagenes/rescatados/bigotes.jpg" >
            <div class="box_data">
                <span>Bigotes</span>
            </div></a>
        </li>
            <li>
            <a href="#0"><img src="imagenes/rescatados/chocolate.jpg">
            <div class="box_data">
                <span>Chocolate</span>
            </div></a>
        </li>
            <li>
            <a href="#0"><img src="imagenes/rescatados/Luna.jpg">
            <div class="box_data">
                <span>Luna</span>
            </div></a>
        </li>
                <li>
            <a href="#0"><img src="imagenes/rescatados/Maya.jpg">
            <div class="box_data">
                <span>Maya</span>
            </div></a>
        </li>
                <li style="    position: relative;
        top: -134px;">
            <a href="#0"><img src="imagenes/rescatados/Oso.jpg">
            <div class="box_data">
                <span>Oso</span>
            </div></a>
        </li>
                <li>
            <a href="#0"><img src="imagenes/rescatados/Pexel.jpg">
            <div class="box_data">
                <span>Pexel</span>
            </div></a>
        </li>
	
    </ul>


    <?php include 'footer.php' ?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="slick/slick.min.js"></script>
<script type="text/javascript" src="js/misperris.js"></script>

</body>
</html>